Vquad
=====

Vquad is a work-in-progress towards a general-purpose, robust, efficient, and parallel library for numerical integration.

History
-------

Vquad is based on algorithm 4 from the article "Increasing the Reliability of Adaptive Quadrature Using Explicit Interpolants", Pedro Gonnet, ACM TOMS 37, 26 (2010).
