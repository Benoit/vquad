# Copyright 2017 Christoph Groth (CEA).
#
# This file is part of Vquad.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.

"""Evaluate an integral using adaptive quadrature.

The algorithm uses Clenshaw-Curtis quadrature rules of increasing
degree in each interval.  The error estimate is
sqrt(integrate((f0(x) - f1(x))**2)), where f0 and f1 are two
successive interpolations of the integrand.  To fall below the
desired total error, intervals are worked on ranked by their own
absolute error: either the degree of the rule is increased or the
interval is split if either the function does not appear to be
smooth or a rule of maximum degree has been reached.

Reference: "Increasing the Reliability of Adaptive Quadrature
    Using Explicit Interpolants", P. Gonnet, ACM Transactions on
    Mathematical Software, 37 (3), art. no. 26, 2008.

Currently the integrator is vectorial, parallel and deterministic

The function must take array as argument
f: array --> array(array)
f(x).shape = (len(x), ...)
______________________

Examples:

>>> integral, error = vquad.vquad(f, [(a, b)], rtol, atol)

-----

>>> integrator = vquad.vquad(f, [(a, b)], rtol, atol, return_obj=True)

>>> integrator(x)              # interpolation of f
>>> integrator.integrate(a, b) # primitive of f, error
>>> integrator.totals()        # integral, error
>>> integrator.points()        # points of evaluation

-----

parallel integration: vquad.Scheduler
"""

from .core import vquad, Vquad, Scheduler