# Copyright 2017, 2018 Christoph Groth (CEA).
#
# This file is part of Vquad.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.

import bisect
import numpy as np
from scipy.linalg import norm
from collections import OrderedDict
from math import sqrt

from . import tables as tbls

eps = np.spacing(1)

# If the relative difference between two consecutive approximations is
# lower than this value, the error estimate is considered reliable.
# See section 6.2 of Pedro Gonnet's thesis.
hint = 0.1

# Smallest acceptable relative difference of points in a rule.  This was chosen
# such that no artifacts are apparent in plots of (i, log(a_i)), where a_i is
# the sequence of estimates of the integral value of an interval and all its
# ancestors..
min_sep = 16 * eps

min_level = 1
max_level = 4

ndiv_max = 20

_sqrt_one_half = np.sqrt(0.5)


def _eval_legendre(c, x):
    """Evaluate _orthonormal_ Legendre polynomial.

    This uses the three-term recurrence relation from page 63 of Perdo
    Gonnet's thesis.
    """
    if len(c[0]) <= 1:
        c0 = c[:, 0]
        c1 = np.zeros(len(c))
    else:
        n = len(c[0])
        c0 = c[:, -2]              # = c[k + 0]
        c1 = np.outer([1]*len(x), c[:, -1])    # = c[k + 1]
        for k in range(len(c[0]) - 2, 0, -1):
            a = (2*k + 3) / (k + 1)**2
            tmp = c0
            c0 = c[:, k - 1] - c1 * sqrt(a * k**2 / (2*k - 1))
            c1 = tmp + (x * c1.T).T * sqrt(a * (2*k + 1))

    return sqrt(1/2) * c0 + (sqrt(3/2) * c1.T * x).T


def _calc_coeffs(vals, level):
    # Vectorial method
    nans = np.array(np.nonzero(~np.isfinite(vals))).T
    if nans.size:
        # Replace vals by a copy and zero-out non-finite elements.
        vals = vals.copy()
        for nan in nans:
            vals[tuple(nan)] = 0
        # Prepare things for the loop further down.
        b = tbls.newton_coeffs[level].copy()
        ms = [len(b) - 2]*vals.shape[0]   # = len(tbls.nodes[level]) - 1'''
    coeffs = (tbls.inv_Vs[level] @ vals.T).T

    # This is a variant of Algorithm 7 from the thesis of Pedro Gonnet where no
    # linear system has to be solved explicitly.  Instead, Algorithm 5 is used.
    for h, i in nans:
        m = ms[h]
        b[m + 1] /= tbls.alpha[m]
        x = tbls.nodes[level][i]
        b[m] = (b[m] + x * b[m + 1]) / tbls.alpha[m - 1]
        for j in range(m - 1, 0, -1):
            b[j] = ((b[j] + x * b[j + 1] - tbls.gamma[j + 1] * b[j + 2])
                    / tbls.alpha[j - 1])
        b = b[1:]

        coeffs[h, :m] -= coeffs[h, m] / b[m] * b[:m]
        coeffs[h, m] = 0
        ms[h] -= 1
    return coeffs


def error(err):
    return np.max(err)


def rshape(vals, points):
    if np.shape(vals)[0] != points.shape[0]:
        raise ValueError(
            "Incorrect values shape, probably need to transpose values")
    return np.reshape(vals, points.shape + (-1,)).T


class DivergentIntegralError(ValueError):
    def __init__(self, msg, igral, err):
        self.igral = igral
        self.err = err
        super().__init__(msg)


class _Terminator:
    __slots__ = ['prev', 'next']


class _Interval:
    __slots__ = ['a', 'b', 'coeffs', 'vals', 'igral', 'err', 'level', 'depth',
                 'ndiv', 'c00', 'unreliable_err', 'prev', 'next']

    def __init__(self, a, b, level, depth):
        self.a = a
        self.b = b
        self.level = level
        self.depth = depth

    def points(self, extra=0):
        a = self.a
        b = self.b
        return (a + b) / 2 + (b - a) * tbls.nodes[self.level + extra] / 2

    def interpolate(self, vals, coeffs_old=None):
        self.vals = vals
        self.coeffs = coeffs = _calc_coeffs(self.vals, self.level)
        if coeffs_old is None:
            coeffs_diff = norm(coeffs, axis=1)
        else:
            coeffs_diff = np.zeros(
                (coeffs.shape[0], max(coeffs_old.shape[1], coeffs.shape[1])),
                vals.dtype)
            coeffs_diff[:, :coeffs_old.shape[1]] = coeffs_old
            coeffs_diff[:, :coeffs.shape[1]] -= coeffs
            coeffs_diff = norm(coeffs_diff, axis=1)
        w = self.b - self.a
        self.igral = w * coeffs[:, 0] * _sqrt_one_half
        self.err = w * coeffs_diff
        self.unreliable_err = (error(coeffs_diff) >
                               hint * error(norm(coeffs, axis=1)))

    def __call__(self, x, coord):
        a = self.a
        b = self.b
        x = (2 * x - (a + b)) / (b - a)
        return _eval_legendre(self.coeffs[coord, :], x)

    def integrate(self, x, y, coord):
        w = y - x
        a = self.a
        b = self.b
        nd = (tbls.nodes[self.level] * w + (x + y - a - b)) / (b - a)
        f = self.coeffs[coord, :] @ tbls.vandermonde(nd).T
        coeffs_new = f @ tbls.inv_Vs[self.level].T
        coeffs_old = f[:, ::2] @ tbls.inv_Vs[self.level - 1].T
        igral = w * coeffs_new[:, 0] * np.sqrt(1/2)
        err = w * norm(coeffs_old
                       - coeffs_new[:, :coeffs_old.shape[1]], axis=1)
        return igral, err

    def split(self):
        """Split this interval in the center into two children.

        This is a coroutine that initially yields an array of x values
        of points to be evaluated.  Once the corresponding values have
        been sent back a tuple containing the child intervals is
        yielded and execution ends.
        """
        m = (self.a + self.b) / 2
        f_center = self.vals[:, (self.vals.shape[1] - 1) // 2]

        depth = self.depth + 1
        children = [_Interval(self.a, m, min_level, depth),
                    _Interval(m, self.b, min_level, depth)]
        points = np.concatenate([child.points()[1:-1] for child in children])
        valss = np.zeros((2, self.vals.shape[0], tbls.sizes[min_level]),
                         dtype=self.vals.dtype)
        valss[:, :, 0] = self.vals[:, 0], f_center
        valss[:, :, -1] = f_center, self.vals[:, -1]
        vals = (yield points).reshape((2*self.vals.shape[0], -1))

        valss[0, :, 1:-1] = vals[::2, :]
        valss[1, :, 1:-1] = vals[1::2, :]

        for child, vals, T in zip(children, valss, tbls.Ts):
            child.interpolate(vals, (T[:, :self.coeffs.shape[1]]
                                     @ self.coeffs.T).T)
            # child.ndiv = self.ndiv + (child.c00 > 2*self.c00) #strange line
            child.ndiv = self.ndiv + 1
            if (np.any(child.ndiv > ndiv_max)
                    and np.any(2*child.ndiv > child.depth)):
                msg = ('Possibly divergent integral in the interval '
                       '[{}, {}]! (h={})')
                raise DivergentIntegralError(
                    msg.format(child.a, child.b, child.b - child.a),
                    child.igral * np.inf, None)
        yield children

    def refine(self):
        """Increase degree of interval.

        This is a coroutine that initially yields an array of x values
        of points to be evaluated.  Once the corresponding values have
        been sent back, a bool is yielded and execution ends.

        It is "true" if further refinements/splits of the interval seem
        promising, and "false" otherwise.  This is the case when
        neigboring points can be resolved only barely by floating point
        numbers, or when the estimated relative error is already at the
        limit of numerical accuracy and cannot be reduced further.
        """
        points = self.points(1)
        vals = np.empty((self.vals.shape[0], points.shape[0]),
                        dtype=self.vals.dtype)
        vals[:, 0::2] = self.vals
        vals[:, 1::2] = (yield points[1::2])

        self.level += 1
        self.interpolate(vals, self.coeffs)

        yield (points[1] - points[0] > points[0] * min_sep
               and points[-1] - points[-2] > points[-2] * min_sep
               and (error(self.err) > (error(abs(self.igral))
                                       * eps * tbls.V_cond_nums[self.level])))


class Vquad:
    """Integrator.

    Example:

    >>> integrator(x)              # interpolation of f
    >>> integrator.integrate(a, b) # primitive of f, error
    >>> integrator.totals()        # integral, error
    >>> integrator.points()        # points of evaluation
    """
    def __init__(self, ivals, atol=0, rtol=0, dim=1, level=max_level-1):
        """Integrator on ivals

        ivals: [(a, b), (c, d), ...]
        atol: absolute tolerance
        rtol: relative tolerence
        dim: (optional) vectorial dimension
        level: initial polynome size (2**(level + 1) + 1)
               between 1 and 4.

        Example:

        >>> vq = vquad.Vquad([[a, b]], atol, rtol)

        >>> while vq.not converged():
        >>>     x = vq.ask()
        >>>     vq.tell(x, f(x))

        or

        >>> for x in vq:
        >>>     vq.tell(x, f(x))
        """

        if rtol < 0 or atol < 0:
            raise ValueError("Tolerances must be positive.")
        if rtol == 0 and atol == 0:
            raise ValueError("Either rtol or atol must be nonzero.")
        if not len(ivals):
            raise ValueError("Empty intervals")
        ivals = sorted(ivals)
        for i, (a, b) in enumerate(ivals):
            if a > b:
                raise ValueError("Interval must be sorted, a <= b")
            if b - a < min_sep:
                raise ValueError("Interval are too small, minimum range %s"
                                 % min_sep)
            if i > 0 and ivals[i - 1][1] > a:
                raise ValueError("Interval must not overlap")

        self._start(rtol, atol, dim)

        prev = self.begin = _Terminator()  # Initialize linked list.
        for a, b in ivals:
            ival = _Interval(a, b, level, 1)
            ival.prev = prev
            prev.next = ival
            prev = ival
            self.ivals[id(ival)] = ival
            self.init[id(ival)] = None

        prev.next = self.end = _Terminator()
        self.end.prev = prev

    def _start(self, rtol, atol, dim):
        "Initialise all value"
        self.rtol = rtol
        self.atol = atol
        self.dim = dim

        self.jobs = {}         # Jobs submitted
        self.working = set()   # Working intervals
        self.workdone = {}     # Values not yet assimiled
        self.ignore = set()    # Interval removed
        self.igral_excess = 0  # igral of removed interval
        self.err_excess = 0    # err of removed interval
        self.ivals = OrderedDict()   # All intervals
        self.init = OrderedDict()    # Interval to init
        self.unrel = set()           # Interval with unreliable err

    def totals(self):
        "Return totals integral and vectorial error"
        igral = 0
        err = 0
        for ival in self.ivals.values():
            if id(ival) in self.init:
                continue
            igral += ival.igral
            err += ival.err
        return igral, err

    def converged(self):
        "Convergence of the current stat"
        if self.init or self.unrel:
            return False
        igral, err = self.totals()
        tol = max(self.atol, error(abs(igral)) * self.rtol)
        return (error(err) == 0 or error(err) < tol
                or (error(self.err_excess) > tol
                    > (error(err) - error(self.err_excess)))
                or len(self.ivals) == 0)

    def pick(self, exclude_job=()):
        """Return expected error and id of the next best interval to improve.
        exclude_job: list of dict of id of ival to exclude
        """
        def keep(Id):
            for d in exclude_job:
                if Id in d:
                    return False
            return True

        if len(self.ivals) == 0:  # All ignored
            return 0, None

        if self.init or self.unrel:
            Ids = ([Id for Id in self.init if keep(Id)] +
                   [Id for Id in self.unrel if keep(Id)])
            if Ids:
                return np.inf, Ids[0]

        idErr = [(id(ival), ival.err)
                 for ival in self.ivals.values() if keep(id(ival))]

        if not idErr:
            return 0, None

        ids, Err = zip(*idErr)
        err = np.sum(Err, axis=0)
        new_err = np.max(err - np.array(Err), axis=1)
        i = np.argmin(new_err)

        return error(err - new_err[i]), ids[i]

    def ask(self, Id=False):
        """Return points to improve.
        Called multiple time ask() well give extra points if possible
        or None.

        Id: if True return id of the interval to improve
        """
        err, id_ival = self.pick([self.ignore, self.working, self.workdone])
        if id_ival is None:
            if Id:
                return None, None
            return None

        ival = self.ivals[id_ival]
        if err == np.inf and id_ival in self.init:  # not initialized
            action = "i"
            operation = None
            points = ival.points()
        elif ival.level == max_level or ival.unreliable_err:
            action = "s"
            operation = ival.split()
            points = next(operation)
        else:
            action = "r"
            operation = ival.refine()
            points = next(operation)

        self.jobs[id(points)] = (action, operation, id_ival)
        self.working.add(id_ival)

        if Id:
            return points, id_ival
        return points

    def tell(self, points, vals):
        """Process the values.
        Points must be .ask() output.
        .ask() and .tell() can be call multiple time in different order
        """
        if points is None:
            return

        if id(points) not in self.jobs:
            raise ValueError("Unknow points")

        vals = rshape(vals, points)
        self.dim = vals.shape[0]

        # save values
        action, operation, id_ival = self.jobs.pop(id(points))
        self.working.remove(id_ival)
        self.workdone[id_ival] = (action, operation, vals)

        while True:  # active interval
            if self.converged():
                return

            err, id_ival = self.pick([self.ignore])
            if id_ival not in self.workdone:
                return

            action, operation, vals = self.workdone.pop(id_ival)
            ival = self.ivals[id_ival]

            if id_ival in self.init:
                self.init.pop(id_ival)
            if id_ival in self.unrel:
                self.unrel.remove(id_ival)

            if action == "r":  # refine
                if not operation.send(vals):
                    # Remove the interval but remember the excess integral and
                    # error.
                    self.err_excess += ival.err
                    self.igral_excess += ival.igral
                    self.ignore.add(id_ival)
                elif ival.unreliable_err:
                    self.unrel.add(id(ival))

            elif action == "s":  # split
                child0, child1 = operation.send(vals)
                self.ivals.pop(id_ival)
                self.ivals[id(child0)] = child0
                self.ivals[id(child1)] = child1

                # Maintain linked list.
                ival.prev.next = child0
                ival.next.prev = child1
                child0.prev = ival.prev
                child0.next = child1
                child1.prev = child0
                child1.next = ival.next

            elif action == "i":  # init
                ival.interpolate(vals)   # Will go away.
                ival.ndiv = 0

    def __iter__(self):
        while not self.converged():
            yield self.ask()

    def points(self):
        """return all points needed for the final integral"""
        points = []
        for ival in self.ivals.values():
            points = points + list(ival.points()[1:-1]) + [ival.a, ival.b]
        return np.unique(points)

    def weights(self):
        """return: points, weights
        integral =  weights @ f(points)
        """
        weights = []
        points = []
        for ival in self.ivals.values():
            points.append(ival.points())
            w = (ival.b - ival.a) * _sqrt_one_half
            weights.append(tbls.inv_Vs[ival.level][0, :] * w)
        return np.concatenate(points), np.concatenate(weights)

    def reset_jobs(self):
        """Reset active jobs"""
        self.working = set()
        self.jobs = {}

    def clean(self):
        """Clean active jobs and non-used values"""
        self.reset_jobs()
        self.workdone = set()

    def new(self, vals):
        """return new integrator
        vals: new values on .points()"""

        xs = self.points()
        if len(vals) != len(xs):
            raise Exception("Shape of new values incompatible")
        vals = rshape(vals, xs)

        vq = Vquad.__new__(Vquad)
        vq._start(self.rtol, self.atol, vals.shape[0])

        iv = self.begin.next
        end = self.end
        prev = vq.begin = _Terminator()
        while iv is not end:
            ival = _Interval(iv.a, iv.b, iv.level, iv.depth)
            vq.ivals[id(ival)] = ival
            if iv in self.init:
                vq.init[id(ival)] = None
            else:
                i = bisect.bisect_left(xs, ival.a)
                j = bisect.bisect(xs, ival.b)
                ival.interpolate(vals[:, i:j])
                _, err = ival.integrate(ival.a, ival.b, coord=slice(None))
                ival.err = err
                ival.unreliable_err = False
            ival.prev = prev
            prev.next = ival
            prev = ival
            iv = iv.next
        prev.next = vq.end = _Terminator()
        vq.end.prev = prev
        return vq

    def __call__(self, xs, coord=slice(None), fill=np.nan):
        """Interpolant f(xs)
        coord: return specific coordinate (slice or array)
        fill: values outside of intergration interval
        """
        xs = np.asarray(xs)
        shape = xs.shape
        if xs.size == 0:
            return np.full(shape, fill)

        xs = xs.flatten()

        # Sort xs, but remember inverse permutation.
        perm = np.argsort(xs)
        xs = xs[perm]
        inv_perm = np.empty(len(perm), int)
        inv_perm[perm] = np.arange(len(perm))

        # Evaluate points interval by interval.
        results = []
        i = 0
        ival = self.begin.next
        end = self.end
        while ival is not end:
            j = bisect.bisect(xs, ival.b, i)
            k = bisect.bisect_left(xs, ival.a, i)
            if k != i:       # Hole in interval
                    results.append(np.full((k-i, self.dim), fill))
            if j != i:
                if id(ival) in self.init:
                    results.append(np.full((j-k, self.dim), fill))
                else:
                    results.append(ival(xs[k:j], coord))
                i = j
            ival = ival.next
        if i < len(xs):
            results.append(np.full((len(xs)-i, self.dim), fill))

        return np.concatenate(results)[inv_perm, :].reshape(shape + (-1,))

    def integrate(self, a, b, coord=slice(None)):
        "Integral of f between a and b"
        ival = self.begin.next
        end = self.end

        s = np.sign(b - a)

        igral = np.zeros(self.dim)
        err = np.zeros(self.dim)

        for ival in self.ivals.values():
            if ival.a <= b and ival.b >= a:
                if id(ival) in self.init:
                    err += np.inf
                    continue
                if ival.a >= a and ival.b <= b:
                    igral = igral + ival.igral[coord]
                    err = err + ival.err[coord]
                else:
                    igl, er = ival.integrate(max(a, ival.a),
                                             min(b, ival.b), coord)
                    igral = igral + igl
                    err = err + er
        return s*igral, err


class Scheduler:
    """Interface for parallel integration

    Example:

    >>> comm = MPI.COMM_WORLD
    >>> if comm.rank == 0:
    >>>     vq = vquad.Vquad([(a, b)], atol, rtol)
    >>>     Sc = vquad.Scheduler(vq)
    >>>
    >>> xs = 0
    >>> t = False       # Sc.converged()
    >>> while not comm.bcast(t, 0):
    >>>     if comm.rank == 0:
    >>>        xs = Sc.ask(comm.size)
    >>>        zs = np.concatenate((xs, [None]*(comm.size - len(xs))))
    >>>     x = comm.scatter(zs, 0)
    >>>     ys = comm.gather(f(x), 0)   # parallel computation
    >>>     if comm.rank == 0:
    >>>         Sc.tell(ys)
    >>>         t = Sc.converged()
    """

    def __init__(self, vq):
        self.vq = vq
        self.workdone = {}
        self.points = []

    def converged(self):
        return self.vq.converged()

    def ask(self, N):
        "Give the next up to N points to integrate"
        if type(N) is not int or N < 1:
            raise Exception("N must be a positive integer")
        self.vq.reset_jobs()
        n = 0
        self.points = []
        while n < N:
            pt, iv = self.vq.ask(True)
            if pt is None:
                break
            points = pt
            if iv in self.workdone:
                points = pt[len(self.workdone[iv]):]
            self.points.append((pt, points, iv))
            n += len(points)
        _, pts, _ = zip(*self.points)
        pts = np.concatenate(pts)
        return pts[:N]

    def tell(self, vals):
        "Append values on previous .ask(N)"
        n = 0
        for pt, point, iv in self.points:
            val = vals[n: min(len(vals), n + len(point))]
            n += len(val)

            if iv in self.workdone:
                val = np.concatenate((self.workdone[iv], val))

            if len(val) == len(pt):
                self.vq.tell(pt, val)
                if iv in self.workdone:
                    self.workdone.pop(iv)
            else:
                self.workdone[iv] = val


def vquad(f, ivals, atol=0, rtol=0, return_obj=False):
    """Integrate f on ivals
    ----
    ivals: [(a, b), (c, d), ...]
    atol: absolute tolerance
    rtol: relative tolerence
    return_obj: return the interpolator

    >>> integrator = vquad.vquad(f, [(a, b)], rtol, atol, return_obj=True)

    >>> integrator(x)              # interpolation of f
    >>> integrator.integrate(a, b) # primitive of f, error
    >>> integrator.totals()        # integral, error
    >>> integrator.points()        # points of evaluation
    """
    igrator = Vquad(ivals, atol, rtol)
    for x in igrator:
        igrator.tell(x, f(x))
    if return_obj:
        return igrator
    return igrator.totals()
